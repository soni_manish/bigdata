1. Log in to Google cloud platform
2. Create cluster 
	#Number of VM to be created
	#Memory
	#And all the necessary configurations to be set etc.
3. Local Machine
 		
 		Install google-cloud-sdk so that the file can be transferred from the local machine to the google cloud(File transferring is not the only goal of downloading the google-cloud-sdk I think :p).

 		1. Create an environment variable 
 			$ export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
 		2. Add the cloud SDK distribution
 			$ echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
 		3. Import the google cloud public key
 			$ curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
 			#If curl is not installed 
 				$ sudo apt-get install curl
 		4. Update
 			$ sudo apt-get update
 		5. Install google sdk
 			$ sudo apt-get install google-cloud-sdk
 		6. $ gcloud init 
 			#This will ask for some details and will ask you to link your google cloud account, GO ahead and give all the details.


 		Example:
 			Create a Scala Hello World Program on the local Machine 
 			Compile it check whether all the .class files are created or no.
 			Make the jar file 

 			$ scalac HelloWorld.scala
 			$ jar cvfe HelloWorld.jar HelloWorld HelloWorld*.class

 		Now transferring the jar file to the google cloud
 		We here transfer jar file to a bucket which we create on the google cloud
 		Bucket is bascially a directory where we can have all our files

 		Google Cloud:
 		
 			Steps:
 			Navigation menu -> Storage - > Browser -> Create bucket

4. Transferring the jar file from local machine to the google cloud
$ gsutil cp HelloWorld.jar gs://<bucket-name>
Ex:
$ gsutil cp HelloWorld.jar gs://big-data-assignment-cluster

5. Now if we want to run this jar file Creat a job
	Google Cloud:
		Navigation menu -> DataProc -> Jobs
		#Fill in all the details
		#Make sure the cluster name is correct
		#Set the appropriate Job type 
			# In our HelloWorld.jar Job Type should be spark
		Once all this is done submit the job
		After submission click on the job id and you can see the output.

Running a word Count program on the Google cloud:

Google Cloud:
1.SSH into the master VM in the cluster
	(A black terminal(I am not being racist here:p) can be seen after the SSH is done )
2. $spark-shell
Scala Program
>scala val text_file = sc.textFile("gs://pub/shakespeare/rose.txt")   [Rose.txt is a file which is present on the public cloud]
(If you want to run the word count file on your own file then you can create it or copy it from the local file to the google cloud bucket and specify its path in sc.textFile()
Ex:
	>scala val text_file = sc.textFile("gs://big-data-assignment-cluster/<InputFile>.txt") 
)
>scala val wordCounts = text_file.flatMap(line => line.split(" ")).map(word =>(word, 1)).reduceByKey((a, b) => a + b)
>scala wordCounts.collect 
	#Output of the word count program can be seen

#Saving the ouput of the wordcount program to the google cloud platform
>scala wordCounts.saveAsTextFile("gs://<bucket-name>/<Output>/")
Ex: 
>scala wordCounts.saveAsTextFile("gs://big-data-assignment-cluster/wordCountOutput/")
>:q

Now the output of the file can be check on the goole cloud platfrom
Open the active shell in your project on google cloud (Right corner)
$ gsutil ls gs://<bucket-name>/<Output>/
Ex:
$ gsutil ls gs://big-data-assignment-cluster/wordCountOutput/
Multiple files can be seen under this bucket 
Ex: part - 00000
	part - 00001
This are basically the output from different worker
Open one of the file and output can be seen

Doneeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee!!!!!!!!!!!!!!!


